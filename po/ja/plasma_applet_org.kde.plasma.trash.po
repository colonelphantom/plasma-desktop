# translation of plasma_applet_trash.po to Japanese
# This file is distributed under the same license as the kdebase package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007, 2008, 2009.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_trash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2022-07-09 13:54+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "開く"

#: contents/ui/main.qml:105
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "空"

#: contents/ui/main.qml:111
#, kde-format
msgid "Trash Settings…"
msgstr "ごみ箱の設定..."

#: contents/ui/main.qml:162
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"ごみ箱\n"
"空"

#: contents/ui/main.qml:162
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"ごみ箱\n"
"%1 アイテム"

#: contents/ui/main.qml:171
#, kde-format
msgid "Trash"
msgstr "ごみ箱"

#: contents/ui/main.qml:172
#, kde-format
msgid "Empty"
msgstr "空"

#: contents/ui/main.qml:172
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "%1 アイテム"
