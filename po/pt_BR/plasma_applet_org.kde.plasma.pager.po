# Translation of plasma_applet_org.kde.plasma.pager.po to Brazilian Portuguese
# Copyright (C) 2013-2019 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2013, 2014, 2016, 2019.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2016, 2017, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.pager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2022-10-19 15:17-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Geral"

#: package/contents/ui/configGeneral.qml:40
#, kde-format
msgid "General:"
msgstr "Geral:"

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgid "Show application icons on window outlines"
msgstr "Mostrar os ícones do aplicativo nos contornos das janelas"

#: package/contents/ui/configGeneral.qml:47
#, kde-format
msgid "Show only current screen"
msgstr "Mostrar apenas na tela atual"

#: package/contents/ui/configGeneral.qml:52
#, kde-format
msgid "Navigation wraps around"
msgstr "Navegação envolvente"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgid "Layout:"
msgstr "Layout:"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgctxt "The pager layout"
msgid "Default"
msgstr "Padrão"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Horizontal"
msgstr "Horizontal"

#: package/contents/ui/configGeneral.qml:66
#, kde-format
msgid "Vertical"
msgstr "Vertical"

#: package/contents/ui/configGeneral.qml:80
#, kde-format
msgid "Text display:"
msgstr "Exibição do texto:"

#: package/contents/ui/configGeneral.qml:83
#, kde-format
msgid "No text"
msgstr "Nenhum texto"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Activity number"
msgstr "Número da atividade"

#: package/contents/ui/configGeneral.qml:91
#, kde-format
msgid "Desktop number"
msgstr "Número da área de trabalho"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Activity name"
msgstr "Nome da atividade"

#: package/contents/ui/configGeneral.qml:99
#, kde-format
msgid "Desktop name"
msgstr "Nome da área de trabalho"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current Activity:"
msgstr "Ao selecionar a atividade atual:"

#: package/contents/ui/configGeneral.qml:113
#, kde-format
msgid "Selecting current virtual desktop:"
msgstr "Ao selecionar a área de trabalho virtual atual:"

#: package/contents/ui/configGeneral.qml:116
#, kde-format
msgid "Does nothing"
msgstr "Não fazer nada"

#: package/contents/ui/configGeneral.qml:124
#, kde-format
msgid "Shows the desktop"
msgstr "Mostra a área de trabalho"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "…and %1 other window"
msgid_plural "…and %1 other windows"
msgstr[0] "...e mais %1 janela"
msgstr[1] "...e mais %1 janelas"

#: package/contents/ui/main.qml:369
#, kde-format
msgid "%1 Window:"
msgid_plural "%1 Windows:"
msgstr[0] "%1 janela:"
msgstr[1] "%1 janelas:"

#: package/contents/ui/main.qml:381
#, kde-format
msgid "%1 Minimized Window:"
msgid_plural "%1 Minimized Windows:"
msgstr[0] "%1 janela minimizada:"
msgstr[1] "%1 janelas minimizadas:"

#: package/contents/ui/main.qml:456
#, kde-format
msgid "Desktop %1"
msgstr "Área de trabalho %1"

#: package/contents/ui/main.qml:457
#, kde-format
msgctxt "@info:tooltip %1 is the name of a virtual desktop or an activity"
msgid "Switch to %1"
msgstr "Mudar para %1"

#: package/contents/ui/main.qml:602
#, kde-format
msgid "Show Activity Manager…"
msgstr "Mostrar o gerenciador de atividades..."

#: package/contents/ui/main.qml:605
#, kde-format
msgid "Add Virtual Desktop"
msgstr "Adicionar uma área de trabalho virtual"

#: package/contents/ui/main.qml:606
#, kde-format
msgid "Remove Virtual Desktop"
msgstr "Remover a área de trabalho virtual"

#: package/contents/ui/main.qml:609
#, kde-format
msgid "Configure Virtual Desktops…"
msgstr "Configurar áreas de trabalho virtuais..."

#~ msgid "Activate %1"
#~ msgstr "Ativar %1"
