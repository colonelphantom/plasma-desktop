# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:07+0000\n"
"PO-Revision-Date: 2023-01-08 16:28+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.0\n"

#: contents/ui/ToolBoxContent.qml:267
#, kde-format
msgid "Choose Global Theme…"
msgstr "Chọn chủ đề toàn cục…"

#: contents/ui/ToolBoxContent.qml:274
#, kde-format
msgid "Configure Display Settings…"
msgstr "Cấu hình thiết lập hiển thị..."

#: contents/ui/ToolBoxContent.qml:295
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Thêm"

#: contents/ui/ToolBoxContent.qml:310
#, kde-format
msgid "Exit Edit Mode"
msgstr "Thoát khỏi chế độ sửa"
